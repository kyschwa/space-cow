//
//  MainMenuScene.swift
//  Space_Cow
//
//  Created by Kyle Schwarzkopf on 3/3/15.
//  Copyright (c) 2015 Kyle Schwarzkopf. All rights reserved.
//

import Foundation
import SpriteKit

class MainMenuScene: SKScene {
    var label = SKLabelNode(fontNamed: "Arial")
    var scoreLabel = SKLabelNode(fontNamed: "Arial")
    var startLabel = SKLabelNode(fontNamed: "Arial")
    
    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed:"Space_Background1")
        background.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        self.addChild(background)
        
        label.fontSize = 100
        label.text = "Space Cow"
        label.fontColor = UIColor(white: 1, alpha: 1.0)
        label.position = CGPoint(x: 1024, y: 1000)
        addChild(label)
        
        
        startLabel.fontSize = 100
        startLabel.fontColor = UIColor(white: 1, alpha: 1.0)
        startLabel.text = "Press Screen to Begin"
        startLabel.position = CGPointMake(self.size.width/2, self.size.height/2)
        addChild(startLabel)
    }
    
    func sceneTapped() {
        let myScene = GameScene(size:self.size)
        myScene.scaleMode = scaleMode
        let reveal = SKTransition.doorwayWithDuration(1.5)
        self.view?.presentScene(myScene, transition: reveal)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        sceneTapped()
    }
   
    
}

