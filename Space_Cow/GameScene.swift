//
//  GameScene.swift
//  Space_Cow
//
//  Created by Kyle Schwarzkopf on 3/3/15.
//  Copyright (c) 2015 Kyle Schwarzkopf. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    let spaceCow: SKSpriteNode = SKSpriteNode(imageNamed: "spaceCow");
    let backgroundMovePointsPerSec: CGFloat = 87.0
    let backgroundLayer = SKNode()
    var dt: NSTimeInterval = 0
    var lastUpdateTime: NSTimeInterval = 0
    let playableRect: CGRect
    
    override init(size: CGSize) {
        let maxAspectRatio:CGFloat = 16.0/9.0 // 1
        let playableHeight = size.width / maxAspectRatio // 2
        let playableMargin = (size.height-playableHeight)/2.0 // 3
        playableRect = CGRect(x: 0, y: playableMargin,
            width: size.width,
            height: playableHeight) // 4
        
        var textures:[SKTexture] = [] // 2
        for i in 1...4 {
            textures.append(SKTexture(imageNamed: "cow\(i)"))
        }
        // 3
        textures.append(textures[2])
        textures.append(textures[1])
        // 4
       // cowAnimation = SKAction.repeatActionForever( SKAction.animateWithTextures(textures, timePerFrame: 0.1))
        
        super.init(size: size) // 5
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented") // 6
    }
    
    func debugDrawPlayableArea() {
        let shape = SKShapeNode()
        let path = CGPathCreateMutable()
        CGPathAddRect(path, nil, playableRect)
        shape.path = path
        shape.strokeColor = SKColor.redColor()
        shape.lineWidth = 4.0
        addChild(shape)
    }
    
    override func didMoveToView(view: SKView) {
    
        backgroundLayer.zPosition = -1
        addChild(backgroundLayer)
        
        playBackgroundMusic("space.mp3")
        
        backgroundColor = SKColor.whiteColor()
        
        for i in 0...1 {
            let background = backgroundNode()
            background.anchorPoint = CGPointZero
            background.position =
                CGPoint(x: CGFloat(i)*background.size.width, y: 0)
            background.name = "space"
            backgroundLayer.addChild(background)
        }
        
        spaceCow.position = CGPoint(x:300, y:750)
        addChild(spaceCow)
        
        runAction(SKAction.repeatActionForever( SKAction.sequence([SKAction.runBlock(spawnAstroid),
            SKAction.waitForDuration(15.0)])))
        runAction(SKAction.repeatActionForever( SKAction.sequence([SKAction.runBlock(spawnSmallAstroid),
            SKAction.waitForDuration(5.0)])))
        runAction(SKAction.repeatActionForever( SKAction.sequence([SKAction.runBlock(spawnMediumAstroid),
            SKAction.waitForDuration(9.0)])))
    }
    
    override func update(currentTime: NSTimeInterval) {
        
        if lastUpdateTime > 0 {
            dt = currentTime - lastUpdateTime
        } else {
            dt = 0
        }
        lastUpdateTime = currentTime
        
        moveBackground()
    }
    
    func spawnAstroid() {
        let enemy = SKSpriteNode(imageNamed: "astroid")
        enemy.name = "astroid"
        
        let enemyScenePos = CGPoint(
            x: size.width + enemy.size.width/2,
            y: CGFloat.random(
                min: CGRectGetMinY(playableRect) + enemy.size.height/2,
                max: CGRectGetMaxY(playableRect) - enemy.size.height/2))
        enemy.position = backgroundLayer.convertPoint(enemyScenePos, fromNode: self)
        
        backgroundLayer.addChild(enemy)
        
        let actionMove = SKAction.moveToX(-enemy.size.width/2, duration: 65.0)
        
        let actionRemove = SKAction.removeFromParent()
        
        enemy.runAction(SKAction.sequence([actionMove, actionRemove]))
    }

    func spawnSmallAstroid() {
        let enemy = SKSpriteNode(imageNamed: "astroid1")
        enemy.name = "astroid1"
        
        let enemyScenePos = CGPoint(
            x: size.width + enemy.size.width/2,
            y: CGFloat.random(
                min: CGRectGetMinY(playableRect) + enemy.size.height/2,
                max: CGRectGetMaxY(playableRect) - enemy.size.height/2))
        enemy.position = backgroundLayer.convertPoint(enemyScenePos, fromNode: self)
        
        backgroundLayer.addChild(enemy)
        
        let actionMove = SKAction.moveToX(-enemy.size.width/2, duration: 36.0)
        
        let actionRemove = SKAction.removeFromParent()
        
        enemy.runAction(SKAction.sequence([actionMove, actionRemove]))
    }
    
    func spawnMediumAstroid() {
        let enemy = SKSpriteNode(imageNamed: "astroid2")
        enemy.name = "astroid2"
        
        let enemyScenePos = CGPoint(
            x: size.width + enemy.size.width/2,
            y: CGFloat.random(
                min: CGRectGetMinY(playableRect) + enemy.size.height/2,
                max: CGRectGetMaxY(playableRect) - enemy.size.height/2))
        enemy.position = backgroundLayer.convertPoint(enemyScenePos, fromNode: self)
        
        backgroundLayer.addChild(enemy)
        
        let actionMove = SKAction.moveToX(-enemy.size.width/2, duration: 47.0)
        
        let actionRemove = SKAction.removeFromParent()
        
        enemy.runAction(SKAction.sequence([actionMove, actionRemove]))
    }
    
    func backgroundNode() -> SKSpriteNode { // 1
        let backgroundNode = SKSpriteNode()
        backgroundNode.anchorPoint = CGPointZero
        backgroundNode.name = "space"
        // 2
        let background1 = SKSpriteNode(imageNamed: "Space_Background1")
        background1.anchorPoint = CGPointZero
        background1.position = CGPoint(x: 0, y: 0)
        backgroundNode.addChild(background1)
        // 3
        let background2 = SKSpriteNode(imageNamed: "Space_Background2")
        background2.anchorPoint = CGPointZero
        background2.position =
            CGPoint(x: background1.size.width, y: 0)
        backgroundNode.addChild(background2)
        // 4
        backgroundNode.size = CGSize(
            width: background1.size.width + background2.size.width,
            height: background1.size.height)
        return backgroundNode
    }
    
    func moveBackground() {
        let backgroundVelocity =
        CGPoint(x: -backgroundMovePointsPerSec, y: 0)
        let amountToMove = backgroundVelocity * CGFloat(dt)
        backgroundLayer.position += amountToMove
        
        backgroundLayer.enumerateChildNodesWithName("space") {
            node, _ in
            let background = node as! SKSpriteNode
            let backgroundScreenPos = self.backgroundLayer.convertPoint(
                background.position, toNode: self)
            if backgroundScreenPos.x <= -background.size.width {
                background.position = CGPoint(
                    x: background.position.x + background.size.width*2,
                    y: background.position.y)
            }
        }
    }
}
